# Environment variables for the Otica framework (scripts, shared config, etc.).
export FRAMEWORK_DIR=${HOME}/bin/otica
export FRAMEWORK_BUCKET=otica
export SCRIPTS_DIR=${FRAMEWORK_DIR}/scripts
export FRAMEWORK_GIT_REMOTE=origin
export FRAMEWORK_GIT_BRANCH=master
