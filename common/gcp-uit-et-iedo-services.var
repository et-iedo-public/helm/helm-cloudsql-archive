# GCLOUD Configuration
export GOOGLE_CLOUD_PROJECT=uit-et-iedo-services
export GCP_PROJECT_ID=${GOOGLE_CLOUD_PROJECT}
export GCP_PROJECT_NAME=${GOOGLE_CLOUD_PROJECT}
export GCP_CONFIGURATION=${GCP_PROJECT_NAME}-${GCP_ENVIRONMENT}
export GCP_REGION=us-west1
export GCP_ZONE=${GCP_REGION}-a
export GCP_ENVIRONMENT=default
export GCP_DNS_DOMAIN=infra.stanford.edu
export GCP_DNS_MANAGED_ZONE=${GCP_PROJECT_ID}-iam-zone
export ACME_DNS_PROVIDER=${GCP_PROJECT_NAME}-dns
export GCP_NAT_TAGS=nat-stanford
export GCP_NAT_IP=35.233.227.217
export GCP_VPC_NAME=services
export GCP_NETWORK=services

# StackDriver Monitoring
export GCP_MONITORING_PROJECT_ID=${GCP_PROJECT_ID}
export GCP_SLACK_CHANNEL=et-iedo-alerts

# Force GCLOUD auth with user credentials
export GCP_USER_AUTH=true

# GCP artifacts bucket
export GCP_ARTIFACTS_BUCKET=${GCP_PROJECT_NAME}-artifacts

# Google group that are granted permissions to GCP resources (iam.tf)
export GCP_WORKGROUP=uit-et-eit_iedo-staff@stanford.edu

# Required by Terraform: APPLICATION_DEFAULT_CREDENTIALS
export TF_VERSION="= 1.0.0"
export GCP_INFRASTRUCTURE_BUCKET=${GCP_PROJECT_ID}-infrastructure
export TF_BACKEND_PREFIX=terraform/${GCP_PROJECT_ID}/${GCP_ENVIRONMENT}/${APP}

# Default Docker registry
export DOCKER_NAMESPACE=${GCP_PROJECT_ID}
export DOCKER_REGISTRY=gcr.io
export DOCKER_REGISTRY_USERNAME=_json_key

# Vault and secrets configuration
export VAULT_PROJECT_NAME=et-iedo
export VAULT_ADDR=https://vault.stanford.edu
export VAULT_AUTH_METHOD=ldap
export VAULT_CACHE=${HOME}/.vault-local
export SEC_PATH=secret/projects/${VAULT_PROJECT_NAME}/${GCP_PROJECT_NAME}
export GCP_KEY_PATH=${SEC_PATH}/common/gcp-provision
export GCP_KEY_FILE=${VAULT_CACHE}/${GCP_KEY_PATH}
export EXTERNAL_DNS_GCP_CREDENTIALS_PATH=${SEC_PATH}/common/dns-admin-key
export EXTERNAL_DNS_DOMAIN_FILTERS=infra.stanford.edu
export DOCKER_REGISTRY_PASSWORD_PATH=${SEC_PATH}/common/gcr-key
export DOCKER_REGISTRY_PASSWORD_PATH_GCR_USER=${SEC_PATH}/common/gcr-user
export DOCKER_REGISTRY_PASSWORD_PATH_GCR_PULL=${SEC_PATH}/common/gcr-pull

# GitLab CI configuration
export GITLAB_SERVER=https://code.stanford.edu
export GITLAB_SEC_FILE=../.gitlab-ci.sec

# Slack
export SLACK_WEBHOOK_PATH=${SEC_PATH}/common/slack/gitlab-integration
export SLACK_GITLAB_CHANNEL=
export SLACK_CICD_CHANNEL=

# Splunk
export SPLUNK_ADDON_SA=${SEC_PATH}/common/splunk-addon-sa
export SPLUNK_SINK_SERVICE_ACCOUNT_PATH=${SEC_PATH}/common/splunk-sink

# Sub-projects dir
export SUB_PROJECTS=sub-projects

# GKE Configuration
export GKE_CLUSTER_NAME=${GCP_ENVIRONMENT}-${GKE_CLUSTER_REGION}
export GKE_CLUSTER_REGION=us-west1
export GKE_CLUSTER_ZONE=${GKE_CLUSTER_REGION}-a
export KUBE_CONTEXT=gke_${GCP_PROJECT_ID}

# Set kube config default namespace
export KUBE_NAMESPACE=${APP_NAMESPACE}

# Internal subnets
export SUBNET_DMZ_PRIMARY_CIDR=10.0.4.0/24
export SUBNET_DEV_PRIMARY_CIDR=10.0.12.0/24
export SUBNET_DEV_SVC_CIDR=10.2.32.0/20
export SUBNET_DEV_POD_CIDR=10.12.0.0/16
export SUBNET_STAGE_PRIMARY_CIDR=10.0.11.0/24
export SUBNET_STAGE_SVC_CIDR=10.2.16.0/20
export SUBNET_STAGE_POD_CIDR=10.11.0.0/16
export SUBNET_PROD_PRIMARY_CIDR=10.0.10.0/24
export SUBNET_PROD_SVC_CIDR=10.2.0.0/20
export SUBNET_PROD_POD_CIDR=10.10.0.0/16

# Reserved CIDRs for GKE masters
# (GKE masters in a private cluster limited to a /28 range).
export GKE_MASTER_CIDR_PROD=172.16.0.16/28
export GKE_MASTER_CIDR_STAGE=172.16.0.32/28
export GKE_MASTER_CIDR_DEV=172.16.0.48/28

# Filestore (Capacity in number of TB)
export FS_CAPACITY=1
export FS_TIER=STANDARD
export FS_NAME=filestore-default
export FS_CIDR=172.16.1.8/29

# Other applications need to know the backup-monitor-user name and email
export BACKUP_MONITOR_USER=backup-monitor-user
export BACKUP_MONITOR_USER_EMAIL=${BACKUP_MONITOR_USER}@${GCP_PROJECT_NAME}.iam.gserviceaccount.com

# Cloud SQL
export MYSQL_SEC_PATH=${SEC_PATH}/cloud-sql/mysql
export MYSQL_MACHINE_TYPE=db-g1-small
export POSTGRES_SEC_PATH=${SEC_PATH}/cloud-sql/postgres
export POSTGRES_MACHINE_TYPE=db-g1-small

# Cloud SQL Proxy
export SQL_PROXY_HOSTNAME=sql-proxy-gcloud-sqlproxy.sql-proxy-${GCP_ENVIRONMENT}
export SQL_PROXY_MYSQL_PORT=3306
export SQL_PROXY_POSTGRES_PORT=5432

# Sometimes all that you need is a DOLLAR (render.sh)
export DOLLAR=$$
