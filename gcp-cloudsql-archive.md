# NAME

gcp-cloudsql-archive.py - Export databases from a GCP Cloud SQL instance

# SYNOPSIS

**gcp-cloudsql-archive.py** [OPTIONS] **\--databases**=inst1:db1,inst2:db2,...  _project_id_ _bucket_

**gcp-cloudsql-archive.py** **\--help**

# DESCRIPTION

The **gcp-cloudsql-archive.py** Python 3 script exports GCP Cloud SQL databases
to a GCP File Storage bucket. It is a thin wrapper around the
"gcloud sql export sql" command. Its main advantage over running
"gcloud sql export sql" directly is it allows the exporting of multiple
databases to separate bucket objects.

The script works by running the "gcloud sql export sql" command in
_asynchronous_ mode, that is, the gcloud command returns immediately and
**gcp-cloudsql-archive.py** checks every so often to see if the export has
finished. See also the **\--timeout-seconds** and **\--sleep-seconds**
options in the "OPTIONS" section below.

Example: In the context of GCP project **my-project** export the databases
**db1** on the server **mysql-1**, and **db2** and **db3** on the server
**mysql-2** from the GCP Cloud SQL instance **mysql-1** to the bucket
`gs://my-bucket`:

    gcp-cloudsql-archive.py --databases=mysql-1:db1,mysql-2:db2,mysql-2:db3 my-project gs://my-bucket

The above command will dump the databases **db1**, **db2**, and **db3** to
the objects `gs://my-bucket/mysql-1/db1.gz`, `gs://my-bucket/mysql-2/db2.gz`,
and `gs://my-bucket/mysql-2/db3.gz` respectively.

Note that the "option" **--databases** is required.

The _bucket_ should be given in the form **gs://bucket**.

# OPTIONS

**\--databases=inst1:db1,inst2:db2,...**

This options is required and should be set equal to the list of databases.
Each expression **inst1:db1** is interpreted to be a database **db1**
running on Cloud SQL instance **inst1**. The database **db-name** will be
exported to the object _bucket_/**inst1**/**db-name**.

**\--offload**

Before exporting the databases create a new instance of the database
instance. This helps reduce load on the running database instance. This is
highly recommended when exporting large databases. This is also called
"serverless export". Note that serverless export costs more than ordinal
export. For more information see Google Cloud Platform's ["Best practices
for importing and exporting
data"](https://cloud.google.com/sql/docs/mysql/import-export).

**\--timeout-seconds**=_number-of-seconds_

If the export takes longer than _number-of-seconds_ to complete raise an error.
Defaults to 3600 seconds.

**\--sleep-seconds**=_number-of-seconds_

Check every _number-of-seconds_ seconds to see if the export has finished.
Defaults to 60 seconds.

**\--dry-run**

Instead of doing the export just print out what would be exported and where.

**\--verbose** | **-v**

Print out extra information while running.

**\--verbosity**=_level_

Pass **\--verbosity**=_level_ to the **gcloud** command. See **gcloud**'s documentation
for the possible values. Defaults to "info".

**-h|\--help**

Show a usage screen and exit.

# EXIT STATUS

Exits with 0 on success, non-zero on error.

# SEE ALSO
gcloud(1)
