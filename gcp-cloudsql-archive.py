#!/usr/bin/env python3
"""
A wrapper around "gcloud sql export sql" so that individual databases
get stored in their own bucket objects.
"""

# pylint: disable=superfluous-parens

import argparse
import datetime
import random
import re
import subprocess
import time

## TYPING
from typing import Tuple
## END OF TYPING

class CloudSQLArchive:
    """
    The main class that does the GCP Cloud SQL backups.
    """

    def __init__(self,
                 project_id:       str,
                 bucket:           str,
                 databases:        list[str],
                 timeout_seconds:  int  = 30 * 60,
                 sleep_seconds:    int  =  1 * 60,
                 offload:          bool = False,
                 verbose:          bool = False,
                 dryrun:           bool = False,
                 verbosity:        bool = False):

        # Strip from parameter "bucket" any trailing slash.
        bucket = re.sub(r"/$", "", bucket)

        self.project_id      = project_id
        self.bucket          = bucket
        self.databases       = databases
        self.timeout_seconds = timeout_seconds
        self.sleep_seconds   = sleep_seconds
        self.offload         = offload
        self.verbose         = verbose
        self.dryrun          = dryrun
        self.verbosity       = verbosity

        self.progress(f"project_id:      {self.project_id}")
        self.progress(f"bucket:          {self.bucket}")
        self.progress(f"databases:       {self.databases}")
        self.progress(f"timeout_seconds: {self.timeout_seconds}")
        self.progress(f"sleep_seconds:   {self.sleep_seconds}")
        self.progress(f"offload:         {self.offload}")
        self.progress(f"verbose:         {self.verbose}")
        self.progress(f"dryrun:          {self.dryrun}")
        self.progress(f"verbosity:       {self.verbosity}")


    def progress(self, msg: str) -> None:
        """
        Print a progress message if in verbose mode.
        """
        if (self.verbose):
            current_utc_datetime = datetime.datetime.now(datetime.timezone.utc)
            current_utc_iso      = current_utc_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')
            print(f"[{current_utc_iso}] progress: {msg}", flush=True)

    @staticmethod
    def dryrun_progress(msg: str) -> None:
        """
        Print a "dry-run" message.
        """
        print(f"DRY-RUN: {msg}")

    def process(self) -> None:
        """
        Backup the databases.
        """
        self.progress("processing...")
        for database in self.databases:
            self.process_database(database)

    def run_command(self, cmd: list[str]) -> Tuple[str, str, int]:

        msg = f"about to run command: {' '.join(cmd)}"
        self.progress(msg)

        result = subprocess.run(
            cmd,
            check=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,  # Decode output as text
        )

        stdout      = result.stdout.strip()
        stderr      = result.stderr.strip()
        return_code = result.returncode

        self.progress(f"stdout:      {stdout}")
        self.progress(f"stderr:      {stderr}")
        self.progress(f"return_code: {return_code}")

        return (stdout, stderr, return_code)

    def get_operational_status(self, instance: str, operational_id: str) -> str:

        cmd = [
            'gcloud',
            'sql',
            'operations',
            'list',
            f"--instance={instance}",
            f"--filter=NAME:{operational_id}",
            "--format=value(STATUS)",
        ]

        (stdout, stderr, _) = self.run_command(cmd)

        if (stderr):
            msg = f"error running command {cmd}: {stderr}"
            raise Exception(msg)

        return stdout

    def process_database(self, database: str, retries: int=3) -> None:
        """Backup a single database with retries.

        Retry the backup on failure up to "retries" times. The wait time
        after each failure is chosen randomly between 30 and 90 minutes.
        We use such a large gap in case the database instance is itself
        undergoing maintenance (i.e., backup)

        If backup fails "retries" times raise an Exception.
        """
        for i in range(retries):
            attempt = i + 1
            msg = f"on attempt {attempt} of backup for database '{database}'"
            self.progress(msg)
            try:
                self.process_database_aux(database)
            except Exception as excpt:
                msg = f"backup attempt {attempt} failed: {excpt}"
                self.progress(msg)

                if (attempt == retries):
                    msg = "this was the last attempt; giving up"
                    raise Exception(msg)

                sleep_minutes =  random.randint(30, 90)
                msg = f"will try again after sleeping for {sleep_minutes} minutes"
                self.progress(msg)
                time.sleep(sleep_minutes * 60)
            else:
                msg = f"backup attempt {attempt} succeeded"
                self.progress(msg)
                break


    def process_database_aux(self, database: str) -> None:
        """Backup a single database to a bucket object.

        The parameter "database" MUST have the format "INSTANCE:DB_NAME".
        For example, "iedo-mysql-1:s_shared_email". If it does not have
        this format this method will raise an exception.

        The name of the object will be "{instance name}/{database
        name}.gz".

        For example, if backing up the "s_shared_email" database from the
        Cloud SQL instance "iedo-mysql-1" the backed-up file will have the
        name "iedo-mysql-1/s_shared_email.gz".

        We run the export _asynchronously_. That is, run the export
        command which returns (almost) instantantly. The command returns a
        string that allows us to check on its progress. We check on its
        progress every few minutes. We timeout ....????

        """
        self.progress(f"processing database '{database}'")

        # Split database into instance and name. The following will
        # raise a ValueError if there is not exactly one ":" in the string
        # database.
        instance, db_name = database.split(':')
        self.progress(f"(instance, db_name): ({instance}, {db_name})")

        archive_file_name = f"{instance}/{db_name}.gz"

        bucket_path = f"{self.bucket}/{archive_file_name}"

        cmd = [
            "gcloud",
            "sql",
            "export",
            "sql",
            instance,
            bucket_path,
            f"--database={db_name}",
            "--async",
            f"--project={self.project_id}",
            f"--verbosity={self.verbosity}"
        ]

        if (self.offload):
            cmd.append("--offload")

        if (self.dryrun):
            # Don't actually run the command.
            CloudSQLArchive.dryrun_progress(f"run command '{' '.join(cmd)}'")
        else:
            # Run the command.
            (stdout, _, _) = self.run_command(cmd)

            # stdout should contain an operational URL of the form
            #   "https://sqladmin.googleapis.com/sql/v1beta4/projects/uit-et-iedo-services/operations/b2d968fe-7756-47f0-9e98-062a00000033"
            # If it doesn't, that is a problem.
            ## regex = rf"^https:.*{self.project_id}/operations/(.*)$"
            regex = re.compile(f"https:.*{self.project_id}/operations/(.*)(?:\s|$)", re.MULTILINE)
            matches = re.findall(regex, stdout)
            if (len(matches) == 1):
                operational_id = matches[0]
                self.progress(f"found operational id '{operational_id}'")
            else:
                msg = f"could not find operational id in '{stdout}'"
                raise Exception(msg)

            start_time = time.time()
            timeout_seconds = self.timeout_seconds
            while (True):
                status = self.get_operational_status(instance, operational_id)
                self.progress(f"current status is {status}")

                elapsed_time = time.time() - start_time
                self.progress(f"elapsed_time is {int(elapsed_time)} seconds")

                match1 = re.match(r'^.*DONE.*$', status, flags=re.IGNORECASE)
                if (match1):
                    # We are done!
                    elapsed_time = time.time() - start_time
                    msg = f"operation finished in {round(elapsed_time, 3)} seconds"
                    self.progress(msg)
                    break

                msg = f"operation not finished (current status is '{status}')"
                self.progress(msg)

                if (int(elapsed_time) > timeout_seconds):
                    msg = f"operation time out (elapsed time {int(elapsed_time)} seconds"
                    raise Exception(msg)

                # Wait a bit
                msg = f"sleeping for {self.sleep_seconds} seconds"
                self.progress(msg)
                time.sleep(self.sleep_seconds)


def main() -> None:
    """
    Called when running as a script.
    """

    description_str = 'Backup Cloud SQL to a Cloud Storage bucket.'
    parser = argparse.ArgumentParser(description=description_str)

    parser.add_argument('project_id', metavar='project-id', nargs=1,
                        help='the GCP project id')
    parser.add_argument('bucket', nargs=1,
                        help='the full URL of the bucket to back up to')

    # Options
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='show more detail when running this script')
    parser.add_argument('--dry-run', action='store_true',
                        help='do a dry run')
    parser.add_argument('--databases', '-d', required=True,
                        help='comma-delimited list of instance:databases')
    parser.add_argument('--verbosity', default="info",
                        help="set verbosity of 'gcloud sql export sql' command")
    parser.add_argument('--offload', action='store_true',
                        help="create a snapshot of the database before exporting (costs extra)")
    parser.add_argument('--timeout-seconds', type=int, default=30*60,
                        help='if export exceeds this limit exit with an error')
    parser.add_argument('--sleep-seconds', type=int, default=1*60,
                        help='number of seconds to wait between checking if export is finished')

    args = parser.parse_args()

    # Parse the list of databases.
    databases = args.databases.split(',')

    csa = CloudSQLArchive(verbose=args.verbose,
                          dryrun=args.dry_run,
                          project_id=args.project_id[0],
                          bucket=args.bucket[0],
                          databases=databases,
                          offload=args.offload,
                          timeout_seconds=args.timeout_seconds,
                          sleep_seconds=args.sleep_seconds,
                          verbosity=args.verbosity,)

    csa.process()

if (__name__ == "__main__"):
    main()
